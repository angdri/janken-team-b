package com.example.jankenteamb.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.jankenteamb.R
import com.example.jankenteamb.model.room.query.JoinAchievementData
import kotlinx.android.synthetic.main.item_achievment_list.view.*

class AchievementAdapter : RecyclerView.Adapter<AchievementAdapter.ViewHolder>(){
    private var listAchievement = mutableListOf<JoinAchievementData>()
    private lateinit var listener : OnClickListenerCallback

    fun setOnClickListener (listenerCallback : OnClickListenerCallback){
        this.listener = listenerCallback
    }

    fun addData(listAchievement: List<JoinAchievementData>){
        this.listAchievement.clear()
        this.listAchievement.addAll(listAchievement)
        notifyDataSetChanged()
    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view){
        fun bind(achievement: JoinAchievementData){
            view.tv_title_achievement.text = achievement.achievement_title
            view.pb_achievement.progress = achievement.achievement_progress
            view.pb_achievement.max = achievement.achievement_max
            view.tv_pb_achievement.text = achievement.achievement_title
            view.tv_coin_achievement.text = achievement.achievement_point.toString()
            if(achievement.achievement_progress == achievement.achievement_max && achievement.achievement_claim != "claimed"){
                view.btn_claim.visibility = View.VISIBLE
                view.btn_claimed.visibility = View.GONE
                listener.let {
                    view.btn_claim.setOnClickListener {
                        listener.onClickListenerCallback(achievement)
                    }
                }
            }else if(achievement.achievement_claim == "claimed"){
                view.btn_claim.visibility = View.GONE
                view.btn_claimed.visibility = View.VISIBLE
            }else{
                view.btn_claim.visibility = View.GONE
                view.btn_claimed.visibility = View.GONE
            }


        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AchievementAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_achievment_list, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = listAchievement.size

    override fun onBindViewHolder(holder: AchievementAdapter.ViewHolder, position: Int) {
        holder.bind(listAchievement[position])
    }

    interface OnClickListenerCallback{
        fun onClickListenerCallback(achievement: JoinAchievementData)
    }
}